# tmux

Tmux - (терминальный мультиплексор) позволяет работать с несколькими сессиями в 1 окне. Вместо нескольких окон терминала к серверу — вы можете использовать одно.

1. `tmux` - запуск нулевой сессии
2. `tmux new -s session1` - новая сессия session1
3. CTRL + b (далее <C-b>) - запустить режим команд
   * c - новое окно
   * w - список окон
   * n - следующее окно
   * p - предыдущее окно
   * 0 (1, 2, 3, ..., n) - переключиться на номер окна
   * " - Деление окна горизонтально (либо команда `tmux split-window -h`)
   * % - Деление окна вертикально (либо команда `tmux split-window -v`)
   * стрелки - Переход между панелей (либо режим мыши)
   * x - закрытие окон (либо команда `exit`)
   * d - отключение от сессии с ее сохранением (сессия не завершается) (либо команда `tmux detach`) 
   * PageUp - режим выхода прокрутки истории (<C-b> c - выход из режима) 
   * , - переименование текущей вкладки
4. `tmux ls` - список сессий
5. `tmux kill-session` - завершить все сессии
6. `tmux kill-session -t session1` - завершить сессию **session1**
7. `tmux list-commands` - список поддерживаемых команд

Нужно узнать как изменять размер панелей

<hr>

[Источник](https://habr.com/ru/post/327630/)

И еще одна [шпаргалка](https://habr.com/ru/post/126996/)

   
