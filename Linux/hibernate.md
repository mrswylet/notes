# hibernate

(https://losst.pro/vklyuchit-gibernatsiyu-ubuntu-16-04)

1. `sudo systemctl hibernate` - проверка работы гибернации.
2. Нужно убедиться, что размер раздел подкачки больше чем кол-во памяти `free -h`.
3. Получаем более подробную информацию об разделе подкачке `swapon --show`, его имя еще понадобится. 
4. Необходимо отредактировать параметры загрузки ядра, но для начала создаем резервную копию файла конфигурации `sudo cp /etc/default/grub /etc/default/grub.back`.
2. Редактируем параметры загрузки ядра `sudo nano /etc/default/grub`. Нужно изменить значение ключа `GRUB_CMDLINE_LINUX_DEFAULT`, добавив `resume=/раздел_подкачки`. Пример: `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash resume=//dev/nvme0n1p2"` (`/swapfile`)
2. `sudo update-grub` - обновление параметр загрузчики Grub
2. `sudo update-initramfs -u -k all` - перегенирация образа initramfs
3. Что бы добавить опцию спящего режима с меню KDE, необходимо отредактировать следующий файл `sudo nano /var/lib/polkit-1/localauthority/50-local.d/com.ubuntu.enable-hibernate.pkla, вписав туда следующее
```
[Re-enable hibernate by default in upower]
Identity=unix-user:*
Action=org.freedesktop.upower.hibernate
ResultActive=yes

[Re-enable hibernate by default in logind]
Identity=unix-user:*
Action=org.freedesktop.login1.hibernate;org.freedesktop.login1.handle-hibernate-key;org.freedesktop.login1;org.free>
ResultActive=yes
```

