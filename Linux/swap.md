# SWAP

* swap - раздел
* swap - файл

В одних источниках говорят, что разници нет, в других, что гибернация не работает на swap-файле.
У меня пока что гибернация не запустилась на файле, а на разделе запустилась.

## Создание swap файла

(Хорошая статься https://losst.pro/fajl-podkachki-linux)

1. Сошдание файла - `sudo fallocate -l 1G /swapfile` (`sudo dd if=/dev/zero of=/swapfile count=42 bs=1G`)

Вместо `dd` лучше использовать утилиту `fallocate`.

`fallocate` - команд для создания файлов, позволяет создавать файлы определенных размеров.
* `-l` - определяет размер создаваемого файла.
    * KB
    * MB
    * GB
    

`dd` - Утилита переносит по одному блоку данных указанного размера с одного места в другое. Фактически, это аналог утилиты копирования файлов `cp` только для блочных данных.
* if - источник копирования;
* of - место назначения;
* count - скопировать указанное количество блоков, размер одного блока указывается в параметре `bs`;
* bs - указывает сколько байт читать и записывать за один раз;
	* с - один символ;
	* b - 512 байт;
	* kB - 1000 байт;
	* K - 1024 байт;
	* MB - 1000 килобайт;
	* M - 1024 килобайт;
	* GB - 1000 мегабайт;
	* G - 1024 мегабайт.

2. Далее корректируем права, что бы только root мог записывать в файл `sudo chmod 600 /swapfile`.
3. `sudo mkswap /swapfile` - делаем из файла файл подкачки
4. `sudo swapon /swapfile` - включаем файл подкачки. `swapoff` - отключение.
5. `sudo swapon --show` - вывод информации об подключеных swap

С этого момента система может использовать файл подкачки, но только до перезагрузки, после придется сного включать вручную. Для автоматического включения подкачки, необходимо сделать запись в `/etc/fstab`.

6. `sudo cp /etc/fstab /etc/fstab.back` - создаем резервную копию.
7. `sudo echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab` - делаем запись в `/etc/fstab` о swap файле, что бы система его включала автоматически.




## Что такое 

* suspend to disk
* шифровка swap
* посмотреть утилиту swapon
* посмотреть утилиту blkid
* посмотреть утилиту swapspace
* 